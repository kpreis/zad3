---
author: Kamil Preis
title: Największa Organizacja
subtitle: Projekt Technologia Informacyjna
date: 7.11.2020
theme: Rzeszów
output: ants_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---

## Slajd nr 1 - Pospolite?

Powstały 110 - 130 milionów lat temu, to tak jak dinozaury!

![image info](./images/stara_mrowka.png)

## Slajd nr 2 - Liczby

Jest ich 10 000 000 000 000 000 (**10 biliardów**)

![image info](./images/mrowisko.png)

Największa superkolonia pokrywa 3700 mil.

## Slajd 3 - Zdolnosci

Mogą zniewolić inne gatunki zmuszając je do pracy w kolonii.

Mrówki zmieniają swoje zachowanie i biorą "wolne", kiedy są wystawione na działanie patogenów, które mogłyby zaszkodzić całej kolonii.

![image info](./images/madra_mrowka.png)

## Slajd 4 - Mrówka - lider

Mrówki szkolą inne mrówki:
* uczą interaktywnie, pokazują po czym czekają na powtórzenie, nadają odpowiednie tempo
* prowadzą inne mrówki w nowe miejsca, które znalazły
* są cierpliwe, czekają na młodszych kolegów

## Slajd 5 - Definicja

**mrówka** - owad z rodziny owadów nalezącej do rzędy błokówek, podrzędu trzonkówek

## Slajd 6 - Budowa
![image info](./images/Budowa-mrowki.jpg)

## Slajd 7 - Organizacja społeczna 

Mrówki tworzą złoone społeczności o wyraźnej strukturze hierarchicznej i ściśle przydzielonych obowiązkach

## Slajd 8 - Zwiadowca
* Idzie pierwsza
* Doskonale przeszukuje nowe obszary nieznane dotąd kolonii
* Jeśli wróci to dopiero wtedy idzie reszta
* jej budowa jest niewielka
* zostawia feromony, po tym inne mrówki rozpoznają czy ten rejon został przebadany

## Slajd 9 - Robotnica
* wyspecjalizowana
* starsze robotnice pracują poza gniazdem
* młodsze pracują w gnieździe
* tworzą *"mrówczy chleb"*
* czyszczą gniazda
* zbierają pokarm

## Slajd 10 - Królowa
* jest największej budowy
* zakłada nową kolonię
* składa jaja
* jest w stanie wejść do innej kolonii i zabić obecną królową
* albo zyc z nią w jednej kolonii
* samiec jest jedynie młodą larwą, po kopulacji umiera

## Slajd 11 - Podsumowanie

\begin{center}"Jedna mrówka na pewno nie jest bystra, ale ich wspólne działania mam ochotę określić jako inteligentne" - *Jurgen Kurths*\end{center}